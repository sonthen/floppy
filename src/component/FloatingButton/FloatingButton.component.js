import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';

import styles from './Button.style';

const FloatingButton = ({text, onPress, customContainerStyle = {}}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.container, customContainerStyle]}>
        <Text>{text}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default FloatingButton;
