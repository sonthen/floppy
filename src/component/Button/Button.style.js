import {StyleSheet} from 'react-native';

import COLOR from '../../utils/constant/color';

export default StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 8,
    backgroundColor: COLOR.PALE_YELLOW,
    height: 54,
    padding: 15,
    justifyContent: 'center',
  },
});
